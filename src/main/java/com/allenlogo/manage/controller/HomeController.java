package com.allenlogo.manage.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2018/11/5
 * @Time 18:09
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class HomeController {

    @RequestMapping("/home")
    public String hone(){
        return "Hello World!";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(name = "name") String name){
        return "Hello "+ name;
    }
}
