package com.allenlogo.manage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2018/11/1
 * @Time 21:19
 * To change this template use File | Settings | File Templates.
 */
@SpringBootApplication
public class Manage extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Manage.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Manage.class, args);
    }
}
